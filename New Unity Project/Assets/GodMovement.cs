﻿using UnityEngine;
using System.Collections;

public class GodMovement : MonoBehaviour
{
	public float moveSpeed = 10;
	Rigidbody rb;
	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKey(KeyCode.W))
		{
			transform.position += transform.forward * moveSpeed;
		}
		if (Input.GetKey(KeyCode.S))
		{
			transform.position -= transform.forward * moveSpeed;
		}
		if (Input.GetKey(KeyCode.A))
		{
			transform.Rotate(0,-moveSpeed * 2,0);
		}
		if (Input.GetKey(KeyCode.D))
		{
			transform.Rotate(0,moveSpeed * 2,0);
		}
	}
}
