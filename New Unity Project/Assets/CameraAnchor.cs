﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections;

public enum PLAYERSTATE
{
	FIRSTPERSON,
	GOD,
	BUILD,
	CHANGINGTOGOD,
	CHANGINGTOPLAYER,
};

public class CameraAnchor : MonoBehaviour
{
	public GameObject playerRef;
	public GameObject godRef;

	public PLAYERSTATE playerState = PLAYERSTATE.FIRSTPERSON;

	private bool coroutineStarted = false;

	public ParticleSystem pSystem;
	public float emissionRate;

	void Start ()
	{
		pSystem.EnableEmission(false);
		emissionRate = pSystem.GetEmissionRate();
		transform.SetParent(playerRef.transform);
		if (playerRef == null)
		{
			Debug.Log("ERROR: PLAYER NOT ASSAIGNED TO CAMERA ANCHOR SCRIPT!");
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.P))
			SwitchPlayerState();

		switch(playerState)
		{
			case PLAYERSTATE.FIRSTPERSON:
				Vector3 followPos = new Vector3(transform.position.x, godRef.transform.position.y, transform.position.z);
				godRef.transform.position = followPos;

				//if (no mouse input to change look at)
				//{
				//	transform.LookAt(Vector3.Lerp(transform.forward, playerRef.transform.forward, 1));
				//}

				break;

			case PLAYERSTATE.GOD:
				break;

			case PLAYERSTATE.CHANGINGTOGOD:
				transform.position = Vector3.Lerp(transform.position, godRef.transform.position, 0.01f);
				transform.LookAt(Vector3.Lerp(transform.forward, playerRef.transform.position, 0.01f));
				float distToTarget = (godRef.transform.position - transform.position).magnitude;
				if (distToTarget < 10)
				{
					transform.position = Vector3.Lerp(transform.position, godRef.transform.position, 0.05f);
					transform.LookAt(Vector3.Lerp(transform.forward, playerRef.transform.position, 0.05f));
				}
				if (distToTarget < 2)
				{
					transform.position = godRef.transform.position;
					playerState = PLAYERSTATE.GOD;
				}
				break;

			case PLAYERSTATE.CHANGINGTOPLAYER:
				//GodToPlayerWait();
				if (coroutineStarted == false)
				{
					StartCoroutine("GodToPlayerWait");
				}
				emissionRate += Time.deltaTime * 100;
				pSystem.SetEmissionRate(emissionRate);
				
				break;		
		}
	}

	void SwitchPlayerState()
	{
		switch (playerState)
		{
			case PLAYERSTATE.FIRSTPERSON:
				playerRef.GetComponent<FirstPersonController>().enabled = false;
				transform.SetParent(godRef.transform);
				godRef.GetComponent<GodMovement>().enabled = true;
				playerState = PLAYERSTATE.CHANGINGTOGOD; break;

			case PLAYERSTATE.GOD:
				transform.SetParent(playerRef.transform);
				godRef.GetComponent<GodMovement>().enabled = false;
				playerState = PLAYERSTATE.CHANGINGTOPLAYER; break;
		}
	}

	IEnumerator GodToPlayerWait()
	{
		coroutineStarted = true;
		pSystem.EnableEmission(true);
		pSystem.transform.position = godRef.transform.position - new Vector3(0,2,0);
		yield return new WaitForSeconds(3.0f);
		Debug.Log("Coroutine done!");

		playerRef.GetComponent<FirstPersonController>().enabled = true;
		transform.position = playerRef.transform.position;
		transform.LookAt(playerRef.transform.forward);
		transform.position += new Vector3(0, 1.353f, 0);
		playerState = PLAYERSTATE.FIRSTPERSON;
		coroutineStarted = false;
		emissionRate = 10;
		pSystem.EnableEmission(false);
	}
}
