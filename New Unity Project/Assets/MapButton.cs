﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MapButton : MonoBehaviour
{
	public RawImage image;
	


	// Use this for initialization
	void Start ()
	{
		if (image == null)
		{
			Debug.Log("No Panel attached to MapButton!");
		}
	}
	
	public void ExpandMap()
	{
		image.rectTransform.localPosition = Vector3.Lerp(image.rectTransform.localPosition, Vector3.zero, 0.5f);
		Debug.Log(image.rectTransform.position);
	}

	//IEnumerable ExpandMapImage()
	//{
	//	//yield return new WaitUntil(() => image.rectTransform.localPosition >= Vector3.zero);
	//}
}
